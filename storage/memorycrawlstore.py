import os
from typing import Dict

from storage.exceptions import CrawlLoopDetected
from .crawlstore import CrawlStore


class MemoryCrawlStore(CrawlStore):
    """An in memmory implementation of the CrawlStore.
    In production this would have more checks 
    to prevent overidding uncrawled pages but that's out of scope.
    """


    _previous_crawl = None
    _next_crawl = None
    
    def next_crawl(self) -> Dict[Dict, Dict]:
        """Get the next crawl to action
        
        Raises:
            CrawlLoopDetected -- If the next crawl is the same as the last one it's a loop
        
        Returns:
            Dict[Dict, Dict] -- [description]
        """


        if not self._previous_crawl:
            # We haven't crawled anything yet
            # START_URL would usually come from somewhere else(DB/RabbitMQ) but for the purposes of this in memory store setup an env works fine
            start_url = os.getenv('START_URL', None)
            self._next_crawl = {'crawl_url': start_url, 'crawl_session': None, 'crawl_type': 'get'}

        if self._next_crawl and self._previous_crawl == self._next_crawl:
            raise CrawlLoopDetected
        
        return {
                'next_crawl': self._next_crawl,
                'previous_crawl': self._previous_crawl,
            }
    
    def crawl_success(self):
        """
            Logging or responses to storage
        """
        pass

    def add_crawl(self, crawl):
        self._previous_crawl = crawl['previous_crawl']
        self._next_crawl = crawl['next_crawl']