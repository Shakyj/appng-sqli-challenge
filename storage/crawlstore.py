class CrawlStore:
    """Crawl store abscract class.
    All other store types should inherit from this.
    """
    
    def next_crawl(self):
        raise NotImplementedError()

    def process_completed_crawl(self):
        raise NotImplementedError()

    def crawl_success(self):
        raise NotImplementedError()
    
    def add_crawl(self, crawl):
        raise NotImplementedError()