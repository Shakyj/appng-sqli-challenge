# appNG SQL Injection Challenge

### Using

- Pull image `docker pull infoslack/dvwa`
- Start dvwa image: `docker run -d -p 80:80 infoslack/dvwa`
- source env: `source env`
- install requirements: `pip install -r requirements.txt`
- run: `python3 run.py`