from vulnerabilitychecker.sqlichecker import SQLIChecker
from storage.memorycrawlstore import MemoryCrawlStore


store = MemoryCrawlStore()

checker = SQLIChecker(store=store)

completed = False
while not completed:
    completed = checker.run()