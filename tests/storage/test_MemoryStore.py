import os

import pytest

from storage.memorycrawlstore import MemoryCrawlStore
from storage.crawlstore import CrawlStore
from storage.exceptions import CrawlLoopDetected

@pytest.fixture
def crawl_store(monkeypatch):
    """Creates a MemoryCrawlStore instance with a START_URL env variable set
    
    Arguments:
        monkeypatch -- pytest monkeypatch https://docs.pytest.org/en/latest/monkeypatch.html
    
    Returns:
        MemoryCrawlStore -- MemoryCrawlStore Instance
    """

    monkeypatch.setenv("START_URL", 'http://localhost/login.php')

    return MemoryCrawlStore()

@pytest.fixture
def crawl_store_no_start(monkeypatch):
    """Creates a MemoryCrawlStore instance that has no START_URL env variable set
    
    Arguments:
        monkeypatch -- pytest monkeypatch https://docs.pytest.org/en/latest/monkeypatch.html
    
    Returns:
        MemoryCrawlStore -- MemoryCrawlStore Instance
    """

    monkeypatch.delenv('START_URL', raising=False)
    return MemoryCrawlStore()

@pytest.fixture
def crawl_store_with_loop():
    """Creates a MemoryCrawlStore instance that has a forced crawl loop
    
    Returns:
        MemoryCrawlStore -- MemoryCrawlStore Instance
    """

    crawl_store = MemoryCrawlStore()

    crawl_store._next_crawl = 'loopy'
    crawl_store._previous_crawl = 'loopy'

    return crawl_store

def test_store_is_child_of_crawl_store():
    assert( issubclass(MemoryCrawlStore, CrawlStore) )

def test_store_gives_start_url(crawl_store):
    env_start_url = os.environ['START_URL']
    assert(env_start_url == crawl_store.next_crawl()['next_crawl']['crawl_url'])

def test_store_gives_none_with_no_start_url(crawl_store_no_start):
    assert(None == crawl_store_no_start.next_crawl()['next_crawl']['crawl_url'])

def test_raises_loop_exception_if_detected(crawl_store_with_loop):
    with pytest.raises(CrawlLoopDetected):
        crawl_store_with_loop.next_crawl()

def test_new_crawl_gets_added_to_crawl(crawl_store_no_start):
    test_crawl = {
        'next_crawl': {'crawl_url': 'http://super.crawl'},
        'previous_crawl': {'crawl_url': 'http://previous.super.crawl'},
    }
    crawl_store_no_start.add_crawl(test_crawl)

    assert(crawl_store_no_start.next_crawl() == test_crawl)