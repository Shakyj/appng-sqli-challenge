import pytest

from storage.crawlstore import CrawlStore

@pytest.fixture
def crawl_store_class():
    return CrawlStore()

def test_store_can_give_urls(crawl_store_class):
    assert hasattr(crawl_store_class, 'next_crawl')

def test_forces_child_to_implement_next_crawl(crawl_store_class):
    with pytest.raises(NotImplementedError):
        crawl_store_class.next_crawl()

def test_store_can_process_completed_crawls(crawl_store_class):
    assert hasattr(crawl_store_class, 'process_completed_crawl')

def test_forces_child_to_implement_process_completed_crawl(crawl_store_class):
    with pytest.raises(NotImplementedError):
        crawl_store_class.process_completed_crawl()

def test_has_function_to_log_crawl_success(crawl_store_class):
    assert hasattr(crawl_store_class, 'crawl_success')

def test_forces_child_to_implement_storing_success(crawl_store_class):
    with pytest.raises(NotImplementedError):
        crawl_store_class.crawl_success()

def test_has_function_to_add_crawls_to_store(crawl_store_class):
    assert hasattr(crawl_store_class, 'add_crawl')

def test_forces_child_to_implement_adding_to_store(crawl_store_class):
    with pytest.raises(NotImplementedError):
        crawl_store_class.add_crawl(None)